#!/usr/bin/make -f
API=./src-backend
WEB=./src-web
PATCHES=./src-patches

API_VERSION=1.17.0
WEB_VERSION=v2.16.1
PATCH_VERSION=master
PATCH_FILE=patches/v2.15.1.patch

CHROOT=stretch-amd64-sbuild

export DEBFULLNAME=Olivier Meunier
export DEBEMAIL=olivier@neokraft.net


.PHONY:default
default:
	# nothing to do


update-src:
	test -d $(API) || git clone https://github.com/dani-garcia/bitwarden_rs.git $(API)
	test -d $(WEB) || git clone https://github.com/bitwarden/web.git $(WEB)
	test -d $(PATCHES) || git clone https://github.com/dani-garcia/bw_web_builds.git $(PATCHES)
	(cd $(API); \
		git reset --hard; \
		git checkout master; \
		git reset --hard; \
		git pull; \
		git checkout $(API_VERSION); \
	)
	(cd $(PATCHES); \
		git reset --hard; \
		git checkout master; \
		git reset --hard; \
		git pull; \
		git checkout $(PATCH_VERSION); \
	)
	(cd $(WEB); \
		git reset --hard; \
		git checkout master; \
		git reset --hard; \
		git pull; \
		git checkout $(WEB_VERSION); \
		git submodule update --init --recursive; \
		git apply ../$(PATCHES)/$(PATCH_FILE); \
	)



dch:
	dch --distribution "unstable" "New upstream release"


chroot:
	schroot -c $(CHROOT) -s /bin/bash


debuild:
	sbuild -c $(CHROOT)
